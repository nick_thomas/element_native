import React from "react";
import { Button, Container, Form, Input } from "semantic-ui-react";
import { credentials } from "./../../credentials";
import * as sdk from "matrix-js-sdk";

const Login = () => {
  const client_credentials = {
    baseUrl: "https://matrix.org",
    userId: credentials.userId,
    accessToken: credentials.accessToken,
  };
  const client = sdk.createClient(client_credentials);
  client.startClient();
  client.once("sync", function (state: any, prevState: any, res: any) {
    console.log(state);
    if (state === "PREPARED") {
      console.log("Ready");
      var content = {
        body: "Hello World Again",
        msgtype: "m.text",
      };

      client
        .sendEvent("!vUsCNApfgqAueWJcyo:matrix.org", "m.room.message", content, "")
        .then((res: any) => {
          // message sent successfully
        })
        .catch((err: Error) => {
          console.log(err);
        });
    }
  });

  return (
    <Container>
      <Form>
        <Form.Field>
          <Input placeholder="Username" />
        </Form.Field>
        <Form.Field>
          <Input placeholder="Password" />
        </Form.Field>
        <Button type="submit">Submit</Button>
        <Button>Reset</Button>
      </Form>
    </Container>
  );
};

export default Login;
